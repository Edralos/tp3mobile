package com.example.tp3mobile;

import android.app.Activity;
import android.content.ContentProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView teamName;
    TextView rank;
    TextView match;
    Team team;
    ImageView image;
    public RowHolder(@NonNull View itemView) {
        super(itemView);
        teamName = itemView.findViewById(R.id.teamName);
        rank = itemView.findViewById(R.id.ranking);
        match = itemView.findViewById(R.id.matchScore);
        image = itemView.findViewById(R.id.teamIcon);
        itemView.setOnClickListener(this);
    }

    public void bindModel(String tName, String rnk, String mtch, Team team){
        teamName.setText(tName);
        rank.setText(rnk);
        match.setText(mtch);
        this.team = team;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(itemView.getContext() , TeamActivity.class);
        intent.putExtra("selectedTeam", team);
        itemView.getContext().startActivity(intent);

    }
}
