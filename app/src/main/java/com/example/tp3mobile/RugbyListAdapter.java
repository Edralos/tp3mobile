package com.example.tp3mobile;

import android.app.Activity;
import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RugbyListAdapter extends RecyclerView.Adapter<RowHolder> {

    List<Team> list;
    SportDbHelper helper;
    public RugbyListAdapter(SportDbHelper helper){
        this.helper =helper;
        list = helper.getAllTeams();
    }
    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return (new RowHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rugby,parent,false)));
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        Match lastMatch = list.get(position).getLastEvent();
        String lastEv = lastMatch.getHomeTeam() + " vs " +  lastMatch.getAwayTeam() + ": " + lastMatch.getHomeScore() +"-"+ lastMatch.getAwayScore();
        holder.bindModel(list.get(position).getName(), Integer.valueOf(list.get(position).getRanking()).toString() , lastEv, list.get(position)) ;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
